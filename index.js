// console.log("Hello World!");

// ARITHMETIC OPERATOS

let x = 1397;
let y = 7831;

// let sum = 1397 + 7831;
let sum = x + y;
console.log("Results of addition operator: " + sum);

 let difference = x - y;
 console.log("Results of subtraction operator: " + difference);

let product = x * y;
console.log("Results of multiplication operator: " + product);

let quotient = x / y;
console.log("Results of division operator: " + quotient);

// mosulus (%)
// gets the remainder from 2 divided values.
let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

// ASSIGNMENT OPERATOR (=)

// Basic Assignment
let assignmentNumber = 8;
assignmentNumber = assignmentNumber + 2;
console.log("Results of addition assignment operator: " + assignmentNumber);

 // shorthand method for assignment operator
assignmentNumber += 2;
console.log("Results of addition assignment operator: " + assignmentNumber);

assignmentNumber -= 2;
console.log("Results of addition assignment operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Results of addition assignment operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Results of addition assignment operator: " + assignmentNumber);

// Multiple operators and parenthesis\

let mdas = 1 + 2 - 3 * 4 / 5
console.log("Result of mdas operation: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
/*
	1. (4/5) = 0.8
	2. 2-3 = -1
	3. -1*0.8 = -0.8
	4. 1+-0.8 = 0.2
*/
console.log("results of pemdas operation: " + pemdas);

pemdas = (1 + (2 - 3)) * (4 /5);
console.log("Result of second pemdas operation: " + pemdas);
/*
	1. 4/5 = 0.8
	2. 2-3 = -1
	3. 1+-1 = 0
	4. 0*0.8 = 0
*/

// Incrementation vs Decrementation
// Incrementation (++)
// Decrementation (--)

let z = 1;

// ++z added 1 to its original value.
let increment = ++z;
console.log("Result of pre-incrementation " + increment);
console.log("Result of pre-incrementation " + z);

increment = z++;
console.log("Result of post-incrementation " + increment);
console.log("Result of post-incrementation " + z);

let decrement = --z;
console.log("Result of pre-decrementation: " + decrement);
console.log("Result of pre-decrementation: " + z);

 decrement = z--;
 console.log("Results of post-decremation: " + decrement);
console.log("Results of post-decremation: " + z);

// Type Coercion
let numA = "10";
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// false = 0
let numE = false + 1;
console.log(numE);

let numF = true + 1;
console.log(numF);

// Comparison Operators

let juan = "juan";

// Equality Operator (==)
// Checks 2 operands if they are equal/have the same content
// May return boolean value

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == "1");
console.log("juan" == "juan");
console.log("juan" == juan);

console.log("------------");
// Inequality Operator (!=)
// ! == not

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != "1");
console.log("juan" != "juan");
console.log("juan" != juan);
console.log("------------");
console.log(0 != "1");

// Strict Equality Operator (===)
// Compares the content and also the data type
console.log(1 === 1);
console.log(1 === 2);
console.log("juan" === juan);

// Strict Inequality (!==)
console.log(1 !== 1);
console.log(1 !== 2);
console.log("juan" !== juan);

// Relational Operator
let a = 50;
let b = 65;

// GT (>) Greater than operator
let isGreaterThan = a > b;
console.log(isGreaterThan);

// LT (<) Less than operator
let isLessThan = a < b;
console.log(isLessThan);

// GTE (>=)
let isGTorEqual = a >= b;
console.log(isGTorEqual);

// LTE (<=)
let isLTorEqual = a <= b;
console.log(isLTorEqual)

let numStr = "30"
console.log(a > numStr);

let str = "twenty";
console.log(b >= str);
// In some events; we can receive NaN
// NaN == Not a Number

// Logical Operators
let isLegalAge = true;
let isRegistered = false;

// Logical AND Operator (&& - Ampersands)
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet);

// Logical OR Operator (|| - Double Pipe)
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Results of logical OR operator: " + someRequirementsMet);

// Logical NOT Operator (! - Exclamation Point)
// Returns Opposite

let someRequirementsNotMet = !isRegistered;
console.log("Results of logical Not operator: " + someRequirementsNotMet);
